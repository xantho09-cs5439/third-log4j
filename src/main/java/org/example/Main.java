package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Main {
    protected static final Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        String currentUser = "Recursive lookups not performed: ${java:version}";
        ThreadContext.put("username", currentUser);

        logger.info("This is a log message.");
        logger.info("Message lookups aren't just disabled - they're removed: ${java:version}");
    }
}
